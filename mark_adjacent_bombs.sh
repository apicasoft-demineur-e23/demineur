#!/usr/bin/env bash

count_adjacent_bombs() {
    compte=0

    for dy in $(seq -1 1); do
        for dx in $(seq -1 1); do
            nx=$(($1 + $dx))
            ny=$(($2 + $dy))
            if [ $nx -ge 0 ] && [ $nx -lt $colonnes ] && [ $ny -ge 0 ] && [ $ny -lt $lignes ]; then
                if [ $(is_bomb $nx $ny) = '1' ]; then
                    compte=$((compte + 1))
                fi
            fi
        done
    done

    echo $compte
}

mark_empty_tiles_with_adjacent_bombs() {
    for y in $(seq 0 $((lignes-1))); do
        for x in $(seq 0 $((colonnes-1))); do
            if [ $(is_bomb $x $y) = '0' ]; then
                count_adjacent_bombs $x $y > "plateau/$x,$y"
            fi
        done
    done
}

# mark_empty_tiles_with_adjacent_bombs