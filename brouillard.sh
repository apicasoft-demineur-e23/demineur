#!/usr/bin/env bash

create_fog() {
    rm -r Brouillard
    mkdir Brouillard

    for i in $(seq 0 $(($colonnes-1)))
    do  
        for j in $(seq 0 $(($lignes-1)))
        do
            echo 0 > Brouillard/$i,$j
        done
    done
}

reveal() {
    if [[ $(count_revealed_cases) = 0 ]]; then
        gentillesse
    fi

	echo 1 > Brouillard/$1,$2
    if [[ $(lire_case $1 $2) = 0 ]]
    then
        for x in -1 0 1
        do
            for y in -1 0 1
            do
                diffX=$(($1+x))
                diffY=$(($2+y))
                if [ $diffX -ge 0 ] && [ $diffX -lt $colonnes ] && [ $diffY -ge 0 ] && [ $diffY -lt $lignes ] && [ $(is_visible $diffX $diffY) = 0 ]
                then
                    reveal $diffX $diffY
                fi
            done
        done
    fi
}

is_visible() {
	cat Brouillard/$1,$2
}

# create_fog
# reveal 0 1
# is_visible 0 1
# is_visible 0 2