#!/usr/bin/env bash

CURSEUR_X=0
CURSEUR_Y=0


get_cursor_x() {
    echo $CURSEUR_X
}

get_cursor_y() {
    echo $CURSEUR_Y
}

get_cursor_position() {
    # Format des fichiers de plateau
    echo "$(get_cursor_x),$(get_cursor_y)"
}


do_cursor_movement() {
    # Recevoir l'input de l'utilisateur
    read -s -N 1 key

    # Déplacer en fonction de l'input
    case $key in
        z) CURSEUR_Y=$((CURSEUR_Y-1)) ;;
        q) CURSEUR_X=$((CURSEUR_X-1)) ;;
        s) CURSEUR_Y=$((CURSEUR_Y+1)) ;;
        d) CURSEUR_X=$((CURSEUR_X+1)) ;;

        # Révéler la case s'il appuie sur Espace
        ' ') reveal $CURSEUR_X $CURSEUR_Y ;;
    esac

    # Limiter les déplacements à l'intérieur du plateau

    if [ $CURSEUR_X -ge $colonnes ]; then
        CURSEUR_X=$((colonnes-1))
    elif [ $CURSEUR_X -lt 0 ]; then
        CURSEUR_X=0
    fi

    if [ $CURSEUR_Y -ge $lignes ]; then
        CURSEUR_Y=$((lignes-1))
    elif [ $CURSEUR_Y -lt 0 ]; then
        CURSEUR_Y=0
    fi
}
