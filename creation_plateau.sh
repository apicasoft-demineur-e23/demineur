#!/usr/bin/env bash

# demander la taille du plateau (read)
echo "Vous allez choisir la taille de votre plateau : "

echo "Veuillez entrer le nombre de lignes souhaitées :"
read lignes

if [[ $lignes -gt 10 ]]; then
    lignes=10
elif [[ $lignes -lt 3 ]]; then
    lignes=3
fi

echo "Veuillez entrer le nombre de colonnes souhaitées : "
read colonnes

if [[ $colonnes -gt 10 ]]; then
    colonnes=10
elif [[ $colonnes -lt 3 ]]; then
    colonnes=3
fi

echo "Vous allez jouer sur un plateau de dimension $lignes x $colonnes"

# stockage de la taille dans un fichier 
# echo -e "m=$lignes\nn=$colonnes" > taille

# demander le nb de mines (read)
# echo "Veuillez entre le nombre de mines:" 
# read mine 
# echo "Vous allez  jouer sur un plateau contenant $mine mines "

creer_plateau() {
    # stocker l'état du plateau dans un fichier
    rm -r plateau
    mkdir plateau

    # creation d'un fichier pour chaque case du tableau
    for ((i=0; i<$lignes; i++)); do
        for ((j=0; j<$colonnes; j++)); do
        # Construit le nom de fichier en utilisant le numéro de ligne et de colonne
        fichier="$i,$j"
        echo "0" > "plateau/$fichier"
        done
    done
}


# fonction lire_case(x,y)  prend un x,y et retourne le contenu de la case 0 si vide 
# qui retourne le contenu de la case 
lire_case() {
    local nom_fichier="plateau/$1,$2"
    # echo "Vous cherchez le fichier $nom_fichier"

    # Vérification de l'existence du fichier
    if [[ -f "$nom_fichier" ]]; then
        var=$(cat "$nom_fichier")
        # echo "Le fichier $nom_fichier existe et contient : $var"
    else
        # echo "Le fichier $nom_fichier n'existe pas."
        var=0
        # echo "La case est vide."
    fi

    echo $var
}


