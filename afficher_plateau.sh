#!/usr/bin/env bash

afficher_case() {
	nature=$(lire_case $1 $2)
	visible=$(is_visible $1 $2)

	if [ $visible = '0' ]
	then
		printf "#"
	elif [ $visible = '1' ]
	then
		case $nature in
			0)
			printf " "
			;;
			1)
			printf "\E[29;1m1"
			;;
			2)
			printf "\E[30;1m2"
			;;
			3)
			printf "\E[31;1m3"
			;;
			4)
			printf "\E[32;1m4"
			;;
			5)
			printf "\E[33;1m5"
			;;
			6)
			printf "\E[34;1m6"
			;;
			7)
			printf "\E[35;1m7"
			;;
			8)
			printf "\E[36;1m8"
			;;
			@)
			printf "\E[41;30m@"
			;;
		esac
		printf "\E[0m"
#		if [ $nature = "@" ]
#		then
#			printf "@"
#		else
#			printf " "
#		fi
	fi	
}

afficher_plateau() {
	cursor_x=$(get_cursor_x)
	cursor_y=$(get_cursor_y)

	clear
	cases_restantes=$(($lignes*$colonnes-$(count_revealed_cases)-$nombre_bombes))
	printf "Il reste $cases_restantes cases à découvrir !\n\n"
	for j in $(seq 0 $(($lignes-1)))
	do
		for i in $(seq 0 $(($colonnes-1)))
		do
			if [ $cursor_x = $i ] && [ $cursor_y = $j ]
			then
				printf "\E[32;1mX\E[0m"
			else
				afficher_case $i $j
			fi
		done
		printf "\n"
	done
}
