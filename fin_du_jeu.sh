#!/usr/bin/env bash

count_revealed_cases() {
    res=0
    etats=$(cat Brouillard/*)

    for etat in $etats
    do
        if [ $etat = 1 ]
        then
            res=$((res+1))
        fi
    done

    echo $res
}

afficher_fin_du_jeu() {
	clear
	for j in $(seq 0 $(($lignes-1)))
	do
		for i in $(seq 0 $(($colonnes-1)))
		do
			if [ $(is_visible $i $j) = 0 ] && [ $(is_bomb $i $j) = 1 ]
			then
				reveal $i $j
			fi
			afficher_case $i $j
		done
		printf "\n"
	done
}

check_end_game() {
    x=$(get_cursor_x)
    y=$(get_cursor_y)
    
    if [[ $(is_visible $x $y) = 1 ]] && [[ $(is_bomb $x $y) = 1 ]]
    then
        afficher_fin_du_jeu 
        echo -e "\E[31;1mVous avez perdu\E[0m"
        exit 0
    elif [ $((lignes*colonnes-nombre_bombes)) -eq $(count_revealed_cases) ]
    then
        afficher_fin_du_jeu
        echo -e "\E[32;1mVous avez gagné\E[0m"
        exit 0
    fi
}
