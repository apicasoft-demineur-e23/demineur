#!/bin/bash

function place_bombs() {
	echo "Donne le nombre de bombes"
	read nombre_bombes

	# Une seule case libre donne la victoire avec la gentillesse
	bombes_max=$((lignes*colonnes-2))
	if [ $nombre_bombes -gt $bombes_max ]
	then
		nombre_bombes=$bombes_max
	elif [ $nombre_bombes = 0 ]
    then
        nombre_bombes=1
    fi

	for n in $(seq 1 $nombre_bombes)
	do 
		bombe_x=$((RANDOM%colonnes))
		bombe_y=$((RANDOM%lignes))
#		echo $bombe_x $bombe_y
		while [ $(is_bomb $bombe_x $bombe_y) = "1" ]
		do 
			bombe_x=$((RANDOM%colonnes))
            bombe_y=$((RANDOM%lignes))
		done

		echo "@" > plateau/$bombe_x,$bombe_y
	done
}

# fonction pour dire si ya une bombe ou pas 
function is_bomb(){
	if [ "$(lire_case $1 $2)" = '@' ]
	then 
		echo "1"
	else 
		echo "0"
	fi
}

# place_bombs
