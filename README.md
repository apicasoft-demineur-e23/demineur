# Présentation du projet 

Le projet est une version du jeu Démineur codé en Bash. 

Pour le démarrer, executez "./main.sh"

On utilise les touches Z Q S D pour se déplacer et espace pour révéler une case. On gagne si toutes les cases sauf les bombes ont été révélées et on perd si on révèle une bombe 


# Description du Script 

- Fichier creation_plateau.sh : Il contient les fonctions responsables de la création du plateau du jeu. 
- Fichier afficher_plateau.sh : Il contient les fonctions permettant d'afficher le plateau.
- Fichier brouillard.sh : Il contient la fonction reveal qui permet de révéler une case, la fonction is_visible qui permet de savoir si la case a été révélée ou pas et la fonction create_fog qui permet de créer le dossier brouillard
- Fichier placement_bombes.sh : Il contient la fonction place_bombs permettant de placer les bombes aléatoirement sur le plateau et la fonction is_bomb qui permet de savoir si une case est bombe ou pas.
- Fichier mark_adjacent_bombs.sh : Il contient la fonction count_adjacent_bombs qui calcule le nombre de bombes adjacentes à une case et la fonction mark_empty_tiles_with_adjacent_bombs qui permet de compter le nombre de bombes autour de chaque case vide du plateau
- Fichier cursor.sh : Il contient les fonctions permettant de déplacer le curseur sur le plateau et récupérer sa position.
- Fichier fin_du_jeu.sh : Il contient les fonctions permettant d'évaluer l'état du jeu pour déterminer si le joueur a gagné ou a perdu sa partie.
- Fichier main.sh : Il contient le code global du jeu exploitant toutes les fonctions que contiennent les fichiers.



Ilyes Ayad - ilyes.ayad@etu.utc.fr 

Philippe Lefebvre - philippe.lefebvre@etu.utc.fr

Ikram Dama - ikram.dama@etu.utc.fr

Nadia El Atia - nadia.el-atia@etu.utc.fr

Claire Malgonne - claire.malgonne@etu.utc.fr
