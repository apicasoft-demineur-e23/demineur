#!/usr/bin/env bash
source creation_plateau.sh
source placement_bombes.sh
source brouillard.sh
source mark_adjacent_bombs.sh
source gentillesse.sh
source cursor.sh
source afficher_plateau.sh
source fin_du_jeu.sh

# creation du plateau
echo "Bienvenue au jeu du Demineur"
echo "Vous allez entrer les paramètres du jeu"

creer_plateau
create_fog

# Placement des bombes
place_bombs
mark_empty_tiles_with_adjacent_bombs

# Boucle de jeu
while :
do
    afficher_plateau
    do_cursor_movement
    check_end_game
done