#!/usr/bin/env bash

gentillesse(){
	#récuperation de la valeur de la case
	x=$(get_cursor_x)
	y=$(get_cursor_y)

	#vérification de la présence d'une bombe
	if [[ $(is_bomb $x $y) = '1' ]]; then
		#la case xy est remise a 0
		echo "0" > "plateau/$x,$y"
		#replacement aléatoire de la bombe

		bombe_x=$((RANDOM%lignes))
		bombe_y=$((RANDOM%colonnes))
		#echo $bombe_x $bombe_y
		while [[ $(lire_case $bombe_x $bombe_y) = "@" ]] || ([[ $bombe_x -eq $x ]] && [[ $bombe_y -eq $y ]])
		do 
			bombe_x=$((RANDOM%lignes))
			bombe_y=$((RANDOM%colonnes)) 
		done
		echo "@" > plateau/$bombe_x,$bombe_y

		mark_empty_tiles_with_adjacent_bombs	
	fi
}
